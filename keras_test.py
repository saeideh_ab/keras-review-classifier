import pandas as pd
import numpy as np
import tensorflow as tf
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras import layers
from keras.backend import clear_session
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import matplotlib.pyplot as plt
plt.style.use('ggplot')


def collect_data():
    filepath_dict = {'yelp': 'data/yelp_labelled.txt',
                     'amazon': 'data/amazon_cells_labelled.txt',
                     'imdb': 'data/imdb_labelled.txt'}

    df_list = []
    for source, filepath in filepath_dict.items():
        df = pd.read_csv(filepath, names=['sentence', 'label'], sep='\t')
        df['source'] = source  # Add another column filled with the source name
        df_list.append(df)

    df = pd.concat(df_list)
    return df


# build embeddings/vectors from text using sklearn methods
def build_vecs(train, test):
    vectorizer = CountVectorizer()
    # vectorizer = TfidfVectorizer(max_df=0.7, min_df=5,
    #                              token_pattern=u'(?ui)\\b\\w*[a-z]+\\w*\\b',
    #                              # max_features=300,
    #                              ngram_range=(1, 2),
    #                              stop_words='english'
    #                              )
    x_train = vectorizer.fit_transform(train)
    x_test = vectorizer.transform(test)
    return x_train, x_test


# build embeddings/vectors from text using keras methods
def build_keras_vecs(train, test, maxlen):
    tokenizer = Tokenizer(num_words=5000)
    tokenizer.fit_on_texts(train)
    x_train = tokenizer.texts_to_sequences(train)
    x_test = tokenizer.texts_to_sequences(test)
    vocab_size = len(tokenizer.word_index) + 1  # Adding 1 because of reserved 0 index
    x_train = pad_sequences(x_train, padding='post', maxlen=maxlen)
    x_test = pad_sequences(x_test, padding='post', maxlen=maxlen)
    return x_train, x_test, vocab_size


#  Define the structure of the neural network 1
def build_keras_model(x_train, x_test, y_train, y_test,
                      input_dim, epochs, batch_size
                      ):
    x_train = x_train.astype(np.float32, copy=True)
    x_test = x_test.astype(np.float32, copy=True)
    y_train = np.array(y_train).astype("float32")
    y_test = np.array(y_test).astype("float32")

    model = Sequential()
    model.add(layers.Dense(10, input_dim=input_dim, activation='relu'))  # input layer
    model.add(layers.Dense(1, activation='sigmoid'))  # output layer
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['acc']
                  )
    print("model summary", model.summary())

    history = model.fit(x_train, y_train,
                        epochs=epochs, verbose=False,
                        validation_data=(x_test, y_test), batch_size=batch_size
                        )
    clear_session()
    loss, accuracy = model.evaluate(x_train, y_train, verbose=False)
    print("Training Accuracy: {:.4f}".format(accuracy))
    loss, accuracy = model.evaluate(x_test, y_test, verbose=False)
    print("Testing Accuracy:  {:.4f}".format(accuracy))

    return model, history


#  Define the structure of the neural network 2
def build_keras_model2(x_train, x_test, y_train, y_test,
                       vocab_size, maxlen, embedding_dim,
                       epochs, batch_size
                       ):

    x_train = x_train.astype(np.float32, copy=True)
    x_test = x_test.astype(np.float32, copy=True)
    y_train = np.array(y_train).astype("float32")
    y_test = np.array(y_test).astype("float32")

    # define model
    model = Sequential()
    model.add(layers.Embedding(input_dim=vocab_size,
                               output_dim=embedding_dim,
                               input_length=maxlen))
    model.add(layers.GlobalMaxPool1D())
    model.add(layers.Dense(10, activation='relu'))
    model.add(layers.Dense(1, activation='sigmoid'))  # output layer
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['acc']
                  )
    print("model summary", model.summary())

    history = model.fit(x_train, y_train,
                        epochs=epochs, verbose=False,
                        validation_data=(x_test, y_test), batch_size=batch_size
                        )
    clear_session()
    loss, accuracy = model.evaluate(x_train, y_train, verbose=False)
    print("Training Accuracy: {:.4f}".format(accuracy))
    loss, accuracy = model.evaluate(x_test, y_test, verbose=False)
    print("Testing Accuracy:  {:.4f}".format(accuracy))

    return model, history


# plot loss and accuracy values
def plot_history(history, save_name):
    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    x = range(1, len(acc) + 1)

    plt.figure(figsize=(12, 5))
    plt.subplot(1, 2, 1)
    plt.plot(x, acc, 'b', label='Training acc')
    plt.plot(x, val_acc, 'r', label='Validation acc')
    plt.title('Training and validation accuracy')
    plt.legend()
    plt.subplot(1, 2, 2)
    plt.plot(x, loss, 'b', label='Training loss')
    plt.plot(x, val_loss, 'r', label='Validation loss')
    plt.title('Training and validation loss')
    plt.legend()
    plt.savefig(save_name)


def run_model1(docs_train, docs_test, y_train, y_test):
    X_train, X_test = build_vecs(docs_train, docs_test)
    print("train shape:", X_train.shape)

    # define hyper parameters
    input_dim = X_train.shape[1]  # Number of features
    epochs = 10
    batch_size = 20

    nn_model, hist = build_keras_model(X_train, X_test, y_train, y_test,
                                       input_dim, epochs, batch_size
                                       )
    plot_history(hist, 'model1_loss')


def run_model2(docs_train, docs_test, y_train, y_test):
    # define hyper parameters
    maxlen = 300
    X_train, \
    X_test, \
    vocab_size = build_keras_vecs(docs_train, docs_test, maxlen)
    embedding_dim = 50
    epochs = 8
    batch_size = 20

    print("train shape:", X_train.shape)
    nn_model2, hist2 = build_keras_model2(X_train, X_test, y_train, y_test,
                                          vocab_size, maxlen, embedding_dim,
                                          epochs, batch_size
                                          )
    plot_history(hist2, 'model2_loss')


if __name__ == '__main__':
    dataset = collect_data()
    docs = dataset['sentence']
    labels = dataset['label']

    docs_train, docs_test, y_train, y_test = train_test_split(
        docs, labels, test_size=0.25, random_state=42)

    # first way/algorithm to classification
    run_model1(docs_train, docs_test, y_train, y_test)

    # second way/algorithm to classification
    run_model2(docs_train, docs_test, y_train, y_test)